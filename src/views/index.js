import { mapActions, mapGetters } from 'vuex';

export default {
  name: 'Weather',
  data(){
    return {
      search: 'Paris'
    }
  },
  methods: {
    ...mapActions({
      getWeather: 'getWeather'
    }),
    searchWeather(){
      this.getWeather(this.search)
    }
  },
  computed:{
    ...mapGetters({
      weather: 'weather',
    }),
  },
  mounted(){
    this.getWeather(this.search)
  }
}