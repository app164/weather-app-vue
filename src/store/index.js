import { createStore } from 'vuex'
import axios from 'axios'

const WEATHER_API= "https://api.openweathermap.org/data/:version/weather?q=:city&units=metric&APPID=:APPID"
const WEATHER_API_VERSION = 2.5
const WEATHER_API_KEY = "f825344b0cf0672c689378549f9868db"

export default createStore({
  state: {
    weather:{
      country: '',
      description: '',
      state: 'day',
      icon: '',
      temp:{
        min: 0,
        max: 0,
        actual: 0,
      },
      humidity: 0,
      wind: 0,
      cloud: 0,
    }
  },
  getters:{
    weather: state => state.weather
  },
  mutations: {
    setWeather(state, weather){
      state.weather = weather
    },
  },
  actions: {
    getWeather({commit},city){
      const url = WEATHER_API
        .replace(':version', WEATHER_API_VERSION)
        .replace(':city', city)
        .replace(':APPID', WEATHER_API_KEY)

      axios.get(url)
      .then(weather => weather.data)
      .then(weather=>{
        commit('setWeather', {
          country: weather.sys.country,
          description: weather.weather[0].description,
          state: weather.weather[0].icon.includes("d") ? 'day' : 'night',
          icon: weather.weather[0].icon,
          temp:{
            min: weather.main.temp_min,
            max: weather.main.temp_max,
            actual: weather.main.temp,
          },
          humidity: weather.main.humidity,
          wind: weather.wind.speed,
          cloud: weather.main.temp,
        })
      })
      .catch(()=>{
        commit('setWeather', {
          country: '',
          description: '',
          state: 'day',
          icon: '',
          temp:{
            min: 0,
            max: 0,
            actual: 0,
          },
          humidity: 0,
          wind: 0,
          cloud: 0,
        })
      })
    },
  },
})
