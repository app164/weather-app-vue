# weather_vue

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
### Projet link to preview
```
https://appweathervue22.herokuapp.com/
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
